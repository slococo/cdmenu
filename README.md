# cdmenu

Patched build of `dmenu`.

## Table of contents
  - [Requirements <a name="requirements"></a>](#requirements-)
  - [Installation <a name="installation"></a>](#installation-)
  - [Usage <a name="usage"></a>](#usage-)
  - [Contributing <a name="contributing"></a>](#contributing-)
  - [License <a name="license"></a>](#license-)

## Requirements <a name="requirements"></a>

In order to build `dmenu` you need the `Xlib` header files.

## Installation <a name="installation"></a>

You need to run:

```bash
make clean install  
```

## Usage <a name="usage"></a>

See the `man` page.

## Patches

- Border.
- Case insensitive.
- Center.

## Contributing <a name="contributing"></a>
PRs are welcome.

## License <a name="license"></a>
[MIT](https://raw.githubusercontent.com/santilococo/cdmenu/master/LICENSE)

